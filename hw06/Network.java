//Chase Clapp
//3/19/2019
//CSE 002 Section 210
//The purpose of this code is to create a network of boxes within a specified frame.
//The variables input will be height of window, length of window, box size, and edge length.
//Should print out boxes of said length with said number of edge pieces between.


//import a new scanner so user can input data
import java.util.Scanner;

public class Network{
  public static void main (String args []){
    Scanner myScanner = new Scanner(System.in);
    
    //declare height variable
    int desiredHeight = 0;
    boolean myHeightTruth = true;
    String junkWord = "";
    //check that height variable works
        do{
      System.out.print("Enter height: ");
      myHeightTruth = myScanner.hasNextInt();
      if (myHeightTruth == true){
        desiredHeight = myScanner.nextInt();
        if (desiredHeight < 1){
          myHeightTruth = false;
          System.out.println("Not a correct number!");
        }
      }
      else if (myHeightTruth == false){
        junkWord = myScanner.next();
        System.out.println("Not a correct number!");
      }
    } while (myHeightTruth == false);
    
    //declare width variable
    int desiredWidth = 0;
    boolean myWidthTruth = true;
    
    //check that width variable works
    do{
      System.out.print("Enter width: ");
      myWidthTruth = myScanner.hasNextInt();
      if (myWidthTruth == true){
        desiredWidth = myScanner.nextInt();
        if (desiredWidth < 1){
          myWidthTruth = false;
          System.out.println("Not a correct number!");
        }
      }
      else if (myWidthTruth == false){
        junkWord = myScanner.next();
        System.out.println("Not a correct number!");
      }
    } while (myWidthTruth == false);
    
    
    //declare square size
    int squareSize = 0;
    boolean squareSizeTruth = true;
    
    //check that square size is appropriate
    do{
      System.out.print("Enter square side length: ");
      squareSizeTruth = myScanner.hasNextInt();
      if (squareSizeTruth == true){
        squareSize = myScanner.nextInt();
        if (squareSize < 1){
          squareSizeTruth = false;
          System.out.println("Not a correct number!");
        }
      }
      else if (squareSizeTruth == false){
        junkWord = myScanner.next();
        System.out.println("Not a correct number!");
      }
    } while (squareSizeTruth == false);
    
    
    //declare edge length
    int edgeLength = 0;
    boolean edgeLengthTruth = true;
    
    //check that edge length is apprppriate
    do{
      System.out.print("Enter edge length: ");
      edgeLengthTruth = myScanner.hasNextInt();
      if (edgeLengthTruth == true){
        edgeLength = myScanner.nextInt();
        if (edgeLength < 1){
          edgeLengthTruth = false;
          System.out.println("Not a correct number!");
        }
      }
      else if (edgeLengthTruth == false){
        junkWord = myScanner.next();
        System.out.println("Not a correct number!");
      }
    } while (edgeLengthTruth == false);
    
    //begin code to print characters
    //initialize variables
    boolean printDash = false;
    int widthPlaceholder = 1;
    int heightPlaceholder = 1;
    //begin looping statements
    for(int i = 1; i <= desiredHeight; ++i){
    	for(int j = 1; j <= desiredWidth; ++j){
    	  if(heightPlaceholder == 1 || heightPlaceholder%squareSize == 0){
          //code for edges
    	    if(widthPlaceholder == 1){
    	      System.out.print("#");
    	    }
    	    else if(widthPlaceholder%squareSize == 0){
    	      System.out.print("#");
    	      widthPlaceholder = 0;
    	      printDash = true;
          }
    	    else{
    	      System.out.print("-");
    	    }
    	    widthPlaceholder++;
    	    if(printDash){
    	      for(int k=1; k <=edgeLength ; ++k){
    	        if(squareSize%2 == 0){
    	          if(heightPlaceholder == squareSize / 2 || heightPlaceholder == (squareSize / 2) + 1){
    	            System.out.print("-");
    	          }
    	          else{
    	            System.out.print(" ");
    	          }
    	        }
    	        else{
    	          if(heightPlaceholder == (squareSize / 2) +1){
    	            System.out.print("-");
    	          }
    	          else{
    	            System.out.print(" ");
    	          }
    	        }
    	      j++;
    	      }
    	      printDash = false;
    	    }
    	  }
      //enter code for box sides, both right/left and 
    	  else{
    	    if(widthPlaceholder == 1){
    	      System.out.print("|");
          }
    	    else if(widthPlaceholder%squareSize == 0){
    	      System.out.print("|");
    	      widthPlaceholder = 0;
    	      printDash = true;
    	    }
    	    else{
    	      System.out.print(" ");
    	    }
    	    widthPlaceholder++;
          //enter code for box side, top/bottom
    	    if(printDash){
    	      for(int k = 1;k <= edgeLength; ++k){
    	        if(squareSize%2 == 0){
    	          if(heightPlaceholder == squareSize / 2 || heightPlaceholder == (squareSize / 2) +1){
    	            System.out.print("-");
    	          }
    	          else{
    	            System.out.print(" ");
    	          }
    	        }
    	        else{
    	          if(heightPlaceholder == (squareSize / 2) + 1){
    	            System.out.print("-");
    	          }
    	          else{
    	            System.out.print(" ");
    	          }
    	        }
    	      j++;
    	      }
    	      printDash = false;
    	    }
    	  }
    	}
    	System.out.println();
    	widthPlaceholder = 1;
      //enter code for box sides again, left and right
    	if(heightPlaceholder%squareSize==0){
    	  for(int k = 1;k <= edgeLength; ++k){
    	    for(int l = 1;l <= desiredWidth; ++l){
    	      if(i == desiredHeight){
    	        return;
    	      }
    	      if(squareSize%2 == 0){
	            if(widthPlaceholder == squareSize / 2 || widthPlaceholder == (squareSize / 2) + 1){
	              System.out.print("|");
	              l++;
	            }
	            else{
	              System.out.print(" ");
	            }
    	      }
    	      else{
    	        if(widthPlaceholder == (squareSize / 2) + 1){
    	          System.out.print("|");
    	          l++;
    	        }
    	        else{
    	          System.out.print(" ");
    	        }
    	      }
    	      if(widthPlaceholder%squareSize == 0){
              for(int m = 1; m <= edgeLength; ++m){
    	          System.out.print(" ");
    	          l++;
    	        }
    	        widthPlaceholder = 0;
    	      }
    	      widthPlaceholder++;
    	    }
    	    System.out.println();
    	  i++;
    	  }
    	  heightPlaceholder = 0;
    	}
    	heightPlaceholder++;
    	widthPlaceholder = 1;
    }
  }
}