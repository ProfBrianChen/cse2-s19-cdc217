//Chase Clapp
//CSE 002 Section 210
//16 April 2019
//The code goes with one of two paths: one where an array is siphoned into another array,
//thus creating one array of combined length, and another path where one of the values in the array
//is eliminated, if the shorten value falls within the array bounds.

import java.util.Random;
import java.util.Arrays;

public class ArrayGames{
	//write method for generating array of random length
	public static int[] generator(){
		Random myRandom = new Random();
		int myLength = myRandom.nextInt(10) + 10;
		int[] myArray = new int[myLength];
		return myArray;
	}
	//create a method for placing random numbers 0-25 inside the array
	public static int[] print(){
		Random myRandom = new Random();
		int[] myArray = generator();
		for (int i = 0; i < myArray.length; i++){
			myArray[i] = myRandom.nextInt(25);
		}
		//sort the array
		Arrays.sort(myArray);
		for (int j = 0; j < myArray.length; j++){
			System.out.print(myArray[j]+" ");
		}
		System.out.println();
		return myArray;
	}
	//write a method for inserting one array into another
	public static int[] insert(int[] firstGiven, int[] secondGiven){
		Random myRandom = new Random();
		int newLength = (firstGiven.length + secondGiven.length);// 10 and 5, 15
		System.out.println("New Length: "+newLength);
		int myReplacement = myRandom.nextInt(firstGiven.length);//6
		System.out.println("My Replacement: "+myReplacement);
		int[] thirdArray = new int[newLength];
		int i = 0;
		int j = 0;
		int k = 0;
		//save first half of first array
		while (i < myReplacement){
			thirdArray[i] = firstGiven[i];
			i++;
		}
		//save second array
		while (i >= myReplacement && i < (myReplacement + secondGiven.length)){
			thirdArray[i] = secondGiven[i - myReplacement];
			i++;
		}
		//save last part of first array
		while (i >= (myReplacement + secondGiven.length) && i < newLength){
			thirdArray[i] = firstGiven[(i - secondGiven.length)];
			i++;
		}
		//print out array
		for (int z = 0; z < newLength; z++){
			System.out.print(thirdArray[z]+" ");
		}
		System.out.println();
		return thirdArray;
	}
	//write method for taking number away, if Shorten value is less than Array length
	public static int[] shorten(int[] givenArray, int givenInt){
		Random myRandom = new Random();
		int myShorten = myRandom.nextInt(25);
		int[] newArray = new int[givenArray.length - 1];
		int i = 0;
		System.out.println("Shorten value: "+myShorten);
		System.out.println("Array length: "+givenArray.length);
		//write if statement for if shorten is less than array length
		if (myShorten < givenArray.length){
			while (i < myShorten){
				newArray[i] = givenArray[i];
				i++;
			}
			//skip the number which is eliminated
			while (i < (givenArray.length - 1)){
				newArray[i] = givenArray[i + 1];
				i++;
			}
			for (int j = 0; j < (givenArray.length - 1); j++){
				System.out.print(newArray[j]+" ");
			}
		}
		System.out.println();
		return newArray;
	}
	//write main method
	public static void main (String[] args){
		Random myRandom = new Random();
		int myChoice = myRandom.nextInt(2);
		//randomly select one of two paths to take
		if (myChoice == 0){
			int[] firstArray = print();
			int[] secondArray = print();
			int[] thirdArray = insert(firstArray, secondArray);
		}
		else if (myChoice == 1){
			int[] otherArray = print();
			int myInt = myRandom.nextInt(otherArray.length);
			int[] lastArray = shorten(otherArray, myInt);
		}
	}
}