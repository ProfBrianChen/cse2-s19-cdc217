//Chase Clapp
//CSE 002 Section 210
//Check
//The purpose of this lab is to get the amount each person in a group needs to pay, including tip,
//from the original total cost. Tip will be calculated in the program, and the amount each person
//needs to pay is even.

import java.util.Scanner;
//imports data from user
public class Check{
  
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in );
    // the scanner allows you to input values in the terminal
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //input the cost of the check
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    //input the percent tip
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; 
    //we want to convert the percentage into decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    //enter the number of people who went out with you
    int numPeople = myScanner.nextInt();
    //declare variables for total cost and cost per person
    double totalCost;
    double costPerPerson;
    //whole dollar amount of cost
    int dollars, 
    dimes, pennies; //for storing digits to the right of the decimal point for the cost$
    //calculate total cost and cost per person
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int) costPerPerson;
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    //print out the final line
    System.out.println("Each person in the group owes $"+dollars+'.'+dimes+pennies);
  }
}