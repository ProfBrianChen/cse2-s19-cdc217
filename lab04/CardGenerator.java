//Chase Clapp
//CSE002 Section 210
//2/15/2019
//Card Generator
//The purpose of the program is to use a random number generator to pick a random
//card from a deck of cards, and print what the random card is.

public class CardGenerator {
  public static void main (String[] args) {
    //begin creating string variables
    String cardNumberRead;
    String cardSuit;
    //pick a random number, 1-52
    int cardNumber = (int) (Math.random() * 51 + 1);
    //set standards for suits
    if (cardNumber >= 1 && cardNumber < 14){
        cardSuit = "Diamonds";
    }
    else if (cardNumber >= 14 && cardNumber < 27){
        cardSuit = "Clubs";
    }
    else if (cardNumber >= 27 && cardNumber < 40){
        cardSuit = "Hearts";
    }
    else {
        cardSuit = "Spades";
    }
    //identify the card remainder, so we can see what number/face the card is
    int cardRem = (cardNumber + 13) % 13;
    switch ( cardRem ){
      case 1:
        cardNumberRead = "2";
      break;
      case 2:
        cardNumberRead = "3";
      break;
      case 3:
        cardNumberRead = "4";
      break;
      case 4:
        cardNumberRead = "5";
      break;
      case 5:
        cardNumberRead = "6";
      break;
      case 6:
        cardNumberRead = "7";
      break;
      case 7:
        cardNumberRead = "8";
      break;
      case 8:
        cardNumberRead = "9";
      break;
      case 9:
        cardNumberRead = "10";
      break;
      case 10:
        cardNumberRead = "Jack";
      break;
      case 11:
        cardNumberRead = "Queen";
      break;
      case 12:
        cardNumberRead = "King";
      break;
      default:
        cardNumberRead = "Ace";
      break;
    }
    //print the results
    System.out.println("You picked the "+cardNumberRead+" of "+cardSuit+".");
  }
}