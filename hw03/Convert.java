//Chase Clapp
//CSE 002 Section 210
//2/11/2019
//This simple program allows the user to take an input number
//of meters and convert the distance to inches. The process will be
//from meters, to feet, to inches.

import java.util.Scanner;
//imports data from user
public class Convert{
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner ( System.in );
    
    System.out.print("Enter the distance in meters: ");
    //fetches data from user, specific number of meters
    double meterCount = myScanner.nextDouble();
    //define total number of inches
    double totalInches;
    //define feet per meter
    double feetPerMeter = 3.28084;
    //define inches per foot
    double inchesPerFoot = 12.0;
    //multiply meters by feet per meter by inches per foot 
    totalInches = meterCount * feetPerMeter * inchesPerFoot;
    //print final
    System.out.printf("%5.2f meters is %8.4f inches.\n",meterCount,totalInches);
  }
}