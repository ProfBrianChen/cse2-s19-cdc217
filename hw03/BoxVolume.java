//Chase Clapp
//CSE 002 Section 210
//2/11/2019
//Th purpose of this program is to print the voume of a box,
//with user-given measurements acquired through the "scanner" command.
//Volume is found by multiplying length, width, and height.

import java.util.Scanner;
//allows imports of user data
public class BoxVolume{
  public static void main(String[] args){
    //create a new scanner
    Scanner myScanner = new Scanner ( System.in );
    //print statement to get box width
    System.out.print("The width of the box is: ");
    //create variable for width
    double boxWidth = myScanner.nextDouble();
    //print statement for box length
    System.out.print("The length of the box is: ");
    //create variable for length
    double boxLength = myScanner.nextDouble();
    //print statement for box height
    System.out.print("The height of the box is: ");
    //create variable for box height
    double boxHeight = myScanner.nextDouble();
    //create variable for box volume
    double boxVolume;
    //do calculations for volume
    boxVolume = boxLength * boxWidth * boxHeight;
    //print final statement
    System.out.printf("The volume inside the box is: %.2f \n",boxVolume);
  }
}