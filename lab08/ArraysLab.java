//Chase Clapp
//CSE 002 Section 210
//11 April 2019
//The purpose of the lab is to show students how to make use of arrays.
//Students will use the arrays and manipulate them to make use of them.

import java.util.Arrays;
import java.util.Random;
import java.lang.Math;

public class ArraysLab{
	
	//write method for range
	public static int[] getRange(int[] givenArray, int givenPlaceholder){
		int[] temp = givenArray;
		Arrays.sort(temp);
		int[] sortedArray = new int[givenPlaceholder];
		for (int j = 0; j < givenPlaceholder; j++){
			sortedArray[j] = temp[j];
		}
		//max minus min
		int range = temp[givenPlaceholder - 1] - temp[0];
		System.out.println("Range: "+range);
		return sortedArray;
	}
	
	//write method for mean
	public static double getMean(int[] givenArray, int givenPlaceholder){
		int sum = 0;
		for (int i = 0; i < givenPlaceholder; i++){
			sum += givenArray[i];
		}
		//total sum divided by number of entries
		double mean = (sum / givenPlaceholder);
		System.out.println("Mean: "+mean);
		return mean;
	}
	
	//write method for standard deviation
	public static void getStdDev(int[] givenArray, int givenPlaceholder, double givenMean){
		double[] tempArray = new double[givenPlaceholder];
		int summation = 0;
		for (int i = 0; i < givenPlaceholder; i++){
			tempArray[i] = (double) givenArray[i] - givenMean;
			summation += tempArray[i];
		}
		//declare variables to be put into standard dev equation
		double square = Math.pow(summation, 2);
		double number = square / (givenPlaceholder - 1);
		double stdDev = Math.sqrt(number);
		System.out.println("Standard Dev = "+stdDev);
	}
	
	//shuffle the sorted array
	public static void shuffle(int[] thisArray){
		Random randomNumber = new Random();
		for (int i=0; i<thisArray.length; i++) {
			int randomPosition = randomNumber.nextInt(thisArray.length);
			int temp = thisArray[i];
			thisArray[i] = thisArray[randomPosition];
			thisArray[randomPosition] = temp;
		}
		for (int k = 0; k < thisArray.length; k++) {
			 System.out.println(thisArray[k]);
		}
	}
	
	//write main method
	public static void main (String [] args){
		
		Random myRandom = new Random();
		//get your initial array
		int placeholder = myRandom.nextInt(50);
		placeholder += 50;
		int[] myArray = new int[placeholder];
		System.out.println("Array size: "+placeholder);
		int randomInputs = 0;
		int variable = 0;
		//put variables into array
		for (int i = 0; i < placeholder; i++){
			randomInputs = myRandom.nextInt(100);
			myArray[i] = randomInputs;
			variable = myArray[i];
			System.out.println(variable);
		}
		
		//run methods
		int[] myNewArray = getRange(myArray, placeholder);
		double myMean = getMean(myArray, placeholder);
		getStdDev(myArray, placeholder, myMean);
		shuffle(myNewArray);
		
	}
	
	
}