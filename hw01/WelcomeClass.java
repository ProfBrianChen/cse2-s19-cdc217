///Chase Clapp
///CSE 002 Section 210
///Welcome Class

public class WelcomeClass{
  
  public static void main(String args []){
    ///prints first line of text (dashes)
    System.out.println("    -----------");
    ///prints second line "welcome"
    System.out.println("    | WELCOME |");
    ///prints third line (dashes)
    System.out.println("    -----------");
    ///prints fourth line (exponential signs)
    System.out.println("  ^  ^  ^  ^  ^  ^");
    ///prints slashes
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    ///prints username and other characters
    System.out.println("<-C--D--C--2--1--7->");
    ///prints slashes
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    ///prints "v"
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Chase Clapp, I am a first-year student intending to");
    System.out.println("study Materials Science, and I am from Northampton, Pennsylvania.");
  }
}