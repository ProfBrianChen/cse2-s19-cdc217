//Chase Clapp

import java.util.Scanner;

public class PatternD{
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in);
    int myNumber = 0;
    boolean myNumberTruth = true;
    String junkWord = "";
    
    //use a do while statement to see if the number is an integer, between (and including) 1 and 10
    do{
      System.out.print("Enter an integer 1-10: ");
      myNumberTruth = myScanner.hasNextInt();
      if (myNumberTruth == true){
        myNumber = myScanner.nextInt();
        if (myNumber > 10 || myNumber < 1){
          myNumberTruth = false;
          System.out.println("Not a correct number!");
        }
      }
      else if (myNumberTruth == false){
        junkWord = myScanner.next();
        System.out.println("Not a correct number!");
      }
    } while (myNumberTruth == false);
    
    int myRem = myNumber;
    int myCount = myNumber;
    //int mySpaces = myNumber - 1;
    int myCountRem = myNumber;
    for (int myLines = 1; myLines < (myNumber + 1); myLines++){
      for (myCount = myCountRem; myCount > 0; myCount--){
        System.out.print(myCount);
        System.out.print(" ");
      }
      System.out.println();
      myCountRem--;
    }
  }
}