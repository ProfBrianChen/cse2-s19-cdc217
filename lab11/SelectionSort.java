//Chase Clapp
//CSE 002 Section 210
//3 May 2019

import java.util.Arrays;
public class SelectionSort{
	
	public static int insertionSort(int[] list){
		System.out.println(Arrays.toString(list));
		int iterations = 0;
		for (int i = 0; i < list.length - 1; i++){
			iterations++;
			int min = list[i];
			int minIndex = i;
			for (int j = i + 1; j < list.length; j++){
				iterations++;
				if (list[j] < min){
					min = list[j];
					minIndex = j;
				}
			}
			if (minIndex != i){
				int temp = list[i];
				list[i] = list[minIndex];
				list[minIndex] = temp;
				System.out.println(Arrays.toString(list));
			}
		}
		return iterations;
	}
	
	public static void main (String[] args){
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = insertionSort(myArrayBest);
		int iterWorst = insertionSort(myArrayWorst);
		System.out.println("Total number of operations performed on best is: "+iterBest);
		System.out.println("Total number of operations performed on worst is: "+iterWorst);
		
		
		
		
	}
}