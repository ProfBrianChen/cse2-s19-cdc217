//Chase Clapp
//

import java.util.Scanner;

public class Area{
  
  public static double circleArea (double givenRadius){
    return givenRadius * givenRadius * 3.14;
  }
  
  public static double rectangleArea (double givenHeight, double givenWidth){
    return givenHeight * givenWidth;
  }
  
  public static double triangleArea (double givenTriHeight, double givenTriWidth){
    return ((givenTriHeight * givenTriWidth) / 2.0);
  }
  
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    String shape = "";
    String junkWord = "";
    boolean myShapeTruth = true;
    //check that shape variable works
    do{
      System.out.print("Enter the shape for which you want the area: ");
      shape = myScanner.nextLine();
      if (shape.equals("triangle") || shape.equals("rectangle") || shape.equals("circle")){
        myShapeTruth = true;
      }
      else {
        myShapeTruth = false;
        if (myShapeTruth == false){
          System.out.println("Not a correct shape!");
          System.out.println("Shape options include: circle, rectangle, and triangle.");
        }
      }
    } while (myShapeTruth == false);

    
    //code for area of a circle
    
    double radius = 0;
    boolean myRadTruth = true;
    double height = 0;
    double width = 0;
    boolean myHeightTruth = true;
    boolean myWidthTruth = true;
    double triHeight = 0;
    double triWidth = 0;
    boolean myTriHeightTruth = true;
    boolean myTriWidthTruth = true;
    
    if (shape.equals("circle")){
      
      do{
        System.out.print("Enter radius length: ");
         myRadTruth = myScanner.hasNextDouble();
        if (myRadTruth == true){
          radius = myScanner.nextDouble();
          if (radius < 0){
            myRadTruth = false;
            System.out.println("Must be a positive number!");
          }
        }
        else if (myRadTruth == false){
          junkWord = myScanner.next();
          System.out.println("Must be a positive number!");
        }
      } while (myRadTruth == false);
      
      double finalArea = circleArea(radius);
      System.out.println("Area of circle is about: "+finalArea);
      
        
    }
    

    
    //code for area of a rectangle
    
    else if (shape.equals("rectangle")){
      do{
        System.out.print("Enter height: ");
         myHeightTruth = myScanner.hasNextDouble();
        if (myHeightTruth == true){
          height = myScanner.nextDouble();
          if (height < 0){
            myHeightTruth = false;
            System.out.println("Must be a positive number!");
          }
        }
        else if (myHeightTruth == false){
          junkWord = myScanner.next();
          System.out.println("Must be a positive number!");
        }
      } while (myHeightTruth == false);
      
      do{
        System.out.print("Enter width: ");
         myWidthTruth = myScanner.hasNextDouble();
        if (myWidthTruth == true){
          width = myScanner.nextDouble();
          if (width < 0){
            myWidthTruth = false;
            System.out.println("Must be a positive number!");
          }
        }
        else if (myWidthTruth == false){
          junkWord = myScanner.next();
          System.out.println("Must be a positive number!");
        }
      } while (myWidthTruth == false);
      
      double finalAreaRect = rectangleArea(height, width);
      System.out.println("Area of rectangle: "+finalAreaRect);
      
    }
    
    
    
    
    else if (shape.equals("triangle")){
      
      do{
        System.out.print("Enter height of triangle, from base: ");
         myTriHeightTruth = myScanner.hasNextDouble();
        if (myTriHeightTruth == true){
          triHeight = myScanner.nextDouble();
          if (triHeight < 0){
            myTriHeightTruth = false;
            System.out.println("Must be a positive number!");
          }
        }
        else if (myTriHeightTruth == false){
          junkWord = myScanner.next();
          System.out.println("Must be a positive number!");
        }
      } while (myTriHeightTruth == false);
      
      
      do{
        System.out.print("Enter base length: ");
         myTriWidthTruth = myScanner.hasNextDouble();
        if (myTriWidthTruth == true){
          triWidth = myScanner.nextDouble();
          if (triWidth < 0){
            myTriWidthTruth = false;
            System.out.println("Must be a positive number!");
          }
        }
        else if (myTriWidthTruth == false){
          junkWord = myScanner.next();
          System.out.println("Must be a positive number!");
        }
      } while (myTriWidthTruth == false);
      
      double finalTriArea = triangleArea(triHeight, triWidth);
      System.out.println("Area of triangle: "+finalTriArea);
      
    }
  }
}