//Chase Clapp

import java.util.Scanner;


public class StringAnalysis{
  

  public static boolean stringCheckAll(String givenStringFull){
    int stringLengthForMethod = givenStringFull.length();
    int numberOfInts = 0;
    char currentPlace = 'a';
    boolean letterQuestion = true;
    boolean hurry = true;
    for(int placeholder = 0; placeholder < stringLengthForMethod; placeholder++){
      currentPlace = givenStringFull.charAt(placeholder);
      letterQuestion = Character.isLetter(currentPlace);
      if (letterQuestion == false){
        numberOfInts++;
      }
    }
    if (numberOfInts > 0){
      hurry = false;
    }
    else if (numberOfInts == 0){
      hurry = true;
    }
    return hurry;
  }

  public static boolean stringCheckPart(String givenStringPart, int givenInt){
    int numberOfInts2 = 0;
    char currentPlace2 = 'a';
    boolean letterQuestion2 = true;
    boolean hurry2 = true;
    for (int placeholder2 = 0; placeholder2 > givenInt; placeholder2++){
      currentPlace2 = givenStringPart.charAt(placeholder2);
      letterQuestion2 = Character.isLetter(currentPlace2);
      if (letterQuestion2 == false){
        numberOfInts2++;
      }
    }
    if (numberOfInts2 > 0){
      hurry2 = false;
    }
    else if (numberOfInts2 == 0){
      hurry2 = true;
    }
    return hurry2;
  }



  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    String givenLetters = "";


    System.out.print("Enter a set of characters or a word: ");
    givenLetters = myScanner.nextLine();

    int stringLength = 0;
    boolean hasFirstAnswer = true;
    String myFirstAnswer = "";
    boolean myIntTruth = true;
    int givenStringLength = givenLetters.length();
    int nextInt = 0;

    do{
      System.out.print("Evaluate all characters? Type Y or N: ");
      myFirstAnswer = myScanner.nextLine();
      if (myFirstAnswer.equals("Y")){
        hasFirstAnswer = true;
        stringLength = givenLetters.length();
      }
      else if (myFirstAnswer.equals("N")){
        hasFirstAnswer = true;
        do{
          System.out.print("Enter number of characters you would like to scan: ");
          myIntTruth = myScanner.hasNextInt();
          if (myIntTruth == true){
            stringLength = myScanner.nextInt();
            if (stringLength < 0){
              myIntTruth = false;
            }
            else if (stringLength >= givenStringLength){
              stringLength = givenStringLength;
            }
          }
          else if (myIntTruth = false){
            System.out.println("Not a correct number!");
          }
        } while (myIntTruth == false);
      }
      else{
        hasFirstAnswer = false;
        System.out.println("Not a correct input.");
      }
    }while (hasFirstAnswer == false);

    boolean myFinalAnswer = true;

    if (stringLength > givenStringLength){
      myFinalAnswer = stringCheckPart(givenLetters, stringLength);
    }
    else if (stringLength == givenStringLength){
      myFinalAnswer = stringCheckAll(givenLetters);
    }
    if (myFinalAnswer == true){
      System.out.println("There were no numbers in your input.");
    }
    else if (myFinalAnswer == false){
      System.out.println("There was at least one number in you input.");
    }
  }
}