//Chase Clapp
//03/05/2019
//HW 05 Reading in Loops
//This program will be asking the user to input certain characteristics of their classes.
//The characteristics include the course number (int), department name (string), the number of times it meets in a week (int), 
//the time the class starts (string), the instructor name (string), and the number of students (int). 
//If the user enters the wrong type, give an error message and ask again.

import java.util.Scanner;

public class Hw05{
  public static void main (String args []){
    Scanner myScanner = new Scanner(System.in);
    //find the first integer (course number)
    System.out.print("Enter the course number as an integer: ");
    int courseNumber = 0;
    boolean myCoNumTruth = myScanner.hasNextInt();
    //do while statement to make sure it is not false
    while (myCoNumTruth == false){
      String junkWord1 = myScanner.next();
      System.out.println("Not an integer!");
      System.out.print("Enter the course number as an integer: ");
      myCoNumTruth = myScanner.hasNextInt();
    }
    courseNumber = myScanner.nextInt();
    //find the department name as a string
    System.out.print("Enter the department name as a string: ");
    String deptName = "";
    boolean deptNameIntCheck = true;
    boolean deptNameDoubleCheck = true;
    String junkWord2 = "";
    boolean myDeptNameTruth = true;

    String junk = myScanner.next();
    deptName = myScanner.nextLine();
    //find the number of meetings as an integer
    System.out.print("Meetings per week as an integer: ");
    int weekMeet = 0;
    boolean myWeekMeetTruth = true;
    //make sure it is an integer
    if (weekMeet == 0){
      myWeekMeetTruth = myScanner.hasNextInt();
    }
    while (myWeekMeetTruth == false){
      String junkWord3 = myScanner.next();
      System.out.print(myWeekMeetTruth);
      System.out.println("Not an integer!");
      System.out.print("Meetings per week as an integer: ");
      myWeekMeetTruth = myScanner.hasNextInt();
    }
    weekMeet = myScanner.nextInt();
    //enter the class time as a string
    System.out.print("Enter the time the class starts as a string (e.g. 9AM): ");
    
    String junk3 = myScanner.next();
    String meetTime = myScanner.nextLine();
    //enter the instructor name as a string
    System.out.print("Enter the instructor name as a string: ");
    String instName = "placeholder";

    String junk2 = myScanner.next();

    instName = myScanner.nextLine();
    //finally, print the number of students as an integer
    System.out.print("Enter the number of students as an integer: ");
    int numStudents = 0;
    boolean myNumStudentsTruth = myScanner.hasNextInt();
    //make sure the students are in integers
    while (myNumStudentsTruth == false){
      String junkWord6 = myScanner.next();
      System.out.println("Not an integer!");
      System.out.print("Enter the number of students as an integer: ");
      myNumStudentsTruth = myScanner.hasNextInt();
    }
    numStudents = myScanner.nextInt();
  }
}