//Chase Clapp
//CSE 002 Section 210
//29 April 2019
//The purpose of this program is to become more acquainted with the idea of sorting.
//It sorts the numbers smallest to largest and sees if the user got a poker Straight, such as 
//a 4, 5,  6, 7, 8.


import java.util.Random;
import java.util.Arrays;

public class Straight{
	
	//write a method to shuffle an array of integers 0-51
	public static int[] shuffle(){
		int[] myArray = new int[52];
		int i = 0;
		for (i = 0; i < myArray.length; i++){
			myArray[i] = i;
		}
		Random myRandom = new Random();
		int j = 0;
		int index = 0;
		for (j = myArray.length - 1; j > 0; j--){
			index = myRandom.nextInt(j + 1);
			int temp = myArray[index];
			myArray[index] = myArray[j];
			myArray[j] = temp;
		}
		return myArray;
	}
	
	//write an array to get the first five integers
	public static int[] firstFive(int[] array){
		int i = 0;
		int[] temp = new int[5];
		for (i = 0; i < 5; i++){
			temp[i] = array[i];
		}
		return temp;
	}
	
	//read the array and change to make it easier to find the final result
	//array makes the numbers from 0-12, which allows us to see
	//if the array has a straight, from these numbers alone
	public static int[] read(int[] array, int k){
		array[k] = (array[k] % 13);
		return array;
	}
	
	//goes through each individual array
	public static int[] call(int[]array){
		int i = 0;
		for (i = 0; i < 5; i++){
			array = read(array, i);
		}
		return array;
	}
	
	//checks each number to see if they are a straight
	public static boolean straight(int[] array){
		boolean myTruth = true;
		int i = 0;
		if (array[i] == array[i + 1] - 1 && array[i] == array[i + 2] - 2 && array[i] == array[i + 3] - 3 && array[i] == array[i + 4] - 4){
			myTruth = true;
		}
		else{
			myTruth = false;
		}
		return myTruth;
	}
	
	//sorts the array to help identify smallest to largest
	public static int[] sort(int[] array){
		for (int j = 1; j < 5; j++){
			int temp = 0;
			int k = j;
			while (k > 0){
				int currentMember = array[k];
				int previousMember = array[k-1];
				if (currentMember < previousMember){
					temp = array[k - 1];
					array[k - 1] = array[k];
					array[k] = temp;
				}
			k--;
			}
		}
		return array;
	}
	
	//main method
	public static void main(String[] args){
		int k = 0;
		int i = 0;
		boolean myTruth = true;
		//loop one million times
		for (i = 0; i < 1000000; i++){
			int[]fiveCards = firstFive(shuffle());
			int[]finalArray = call(fiveCards);
			int[] sortedArray = sort(finalArray);
			myTruth = straight(sortedArray);
			if (myTruth == true){
				k++;
			}
		}
		System.out.println("Odds out of 1,000,000: "+k);
	}
}