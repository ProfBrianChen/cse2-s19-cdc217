//Chase Clapp
//CSE 002 Section 210
//28 April 2019
//The purpose of this homeowrk is to gain experience in multidimensional arrays. A city with
//a random length and width between 10 and 15 is to have random populations within each "block"
//between 100 and 999 people. It is to be invaded by robots whose locations are represented by 
//negative numbers and they are to move eastward five times.

import java.util.Random;

public class RobotCity{
	//build city in this method
	public static int[][] buildCity(){
		Random myRandom = new Random();
		int width = (myRandom.nextInt(5) + 11);
		int length = (myRandom.nextInt(5) + 11);
		int[][] cityArray = new int[width][length];
		int i = 0;
		int j = 0;
		int population = 0;
		//give each block a random population
		for (i = 0; i < width; i++){
			for (j = 0; j < length; j++){
				population = (myRandom.nextInt(900) + 100);
				cityArray[i][j] = population;
			}
		}
		return cityArray;
	}
	
	//build a display on the display screen with this method
	public static void display(int[][] cityArray){
		int i = 0;
		int j = 0;
		for (i = (cityArray.length - 1); i >= 0; i--){
			//implement proper spacing within the display
			for (j = 0; j < cityArray[0].length; j++){
				System.out.printf("%4d ", cityArray[i][j]);
			}
			System.out.println();
		}
	}
	
	//invade the city with robots
	public static int[][] invade(int k, int[][] cityArray){
		Random myRandom = new Random();
		int width = 0; 
		int length = 0;
		boolean myTruth = false;
		while (k > 0){
			while (myTruth == false){
				width = myRandom.nextInt(cityArray.length);
				length = myRandom.nextInt(cityArray[0].length);
				if (cityArray[width][length] > 0){
					//robots represented as negative values
					cityArray[width][length] *= -1;
					myTruth = true;
				}
				else if (cityArray[width][length] < 0){
					myTruth = false;
				}
			}
			myTruth = false;
			k--;
		}
		System.out.println("Invade: ");
		int i = 0;
		int j = 0;
		//print out the invaded city
		for (i = (cityArray.length - 1); i >= 0; i--){
			for (j = 0; j < cityArray[0].length; j++){
				System.out.printf("%4d ", cityArray[i][j]);
			}
			System.out.println();
		}
		return cityArray;
	}
	
	//write a method which updates the city as the invaders move
	public static int[][] update(int[][] cityArray){
		int i = 0;
		int j = 0;
		for (i = 0; i < cityArray.length; i++){
			for (j = (cityArray[0].length - 1); j >= 0; j--){
				if (cityArray[i][j] < 0){
					cityArray[i][j] *= -1;
					if(j < (cityArray[0].length - 1)){
						cityArray[i][j + 1] *= -1;
					}
				}
			}
		}
		return cityArray;
	}
	
	//main method
	public static void main (String[] args){
		int[][] cityArray = buildCity();
		System.out.println("City Array: ");
		display(cityArray);
		Random myRandom = new Random();
		int k = myRandom.nextInt(21);
		System.out.println();
		int[][] newArray = invade(k, cityArray);
		for (int i = 1; i < 6; i++){
			System.out.println();
			System.out.printf("Update %d: ",i);
			System.out.println();
			newArray = update(newArray);
			display(newArray);
		}
	}
}
