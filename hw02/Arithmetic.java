//Chase Clapp
//CSE 002 Section 210
//Arithmetic Calculations
//This homework uses various operations and arithmetic methods, including manipulating variables,
//running calculations, and printing answers.

public class Arithmetic {
  public static void main(String[] args){
    //input given data
    //number of pants
    int numPants = 3;
    //cost per pair of pants
    double pantsPrice = 34.98;
  
    //number of sweatshirts
    int numShirts = 2;
    //cost per shirt
    double shirtPrice = 24.99;
  
    //number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
  
    //tax rate
    double paSalesTax = .06;
      
    //specify variables we need (for cost of each kind of item: pants, shirt, belt)
    double pantCost, shirtCost, beltPrice;
    //specify variables we need (sales tax for each item: pants, shirt, belt)
    double pantTax, shirtTax, beltTax;
    //specify totals (before tax, the tax itself, and total with tax)
    double totalBeforeTax, totalTax, totalWithTax;
  
    //begin calculations for pants (cost, tax)
    pantCost = pantsPrice * numPants;
    pantTax = pantCost * paSalesTax;
    //begin calculations for sweatshirts (cost, tax)
    shirtCost = shirtPrice * numShirts;
    shirtTax = shirtCost * paSalesTax;
    //begin calculations for belt (cost, tax)
    beltPrice = numBelts * beltCost;
    beltTax = beltPrice * paSalesTax;
    //begin total calculations
    totalBeforeTax = pantCost + shirtCost + beltPrice;
    totalTax = pantTax + shirtTax + beltTax;
    totalWithTax = totalBeforeTax + totalTax;
  
    //begin printing answers (pants costs, shirts costs, belt costs, and totals)
    System.out.printf("Total cost of pants before tax is $%4.2f and tax associated is $%4.2f.\n",pantCost,pantTax);
    System.out.printf("Total cost of shirts before tax is $%4.2f and tax associated is $%4.2f.\n",shirtCost,shirtTax);
    System.out.printf("Total cost of belt before tax is $%4.2f and tax associated is $%4.2f.\n",beltCost,beltTax);
    System.out.printf("Total cost before tax is $%4.2f, total tax is $%4.2f, and total overall is $%4.2f.\n",totalBeforeTax,totalTax,totalWithTax);
  }
}