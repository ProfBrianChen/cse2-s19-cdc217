//Chase Clapp
//CSE 002 Section 210
//TwistGenerator lab 06
//This program will be writing "twists" of text with ( / \ x ) based on user inputs. 
//The user must input a positive integer or it will ask the user to put in one again.
//The twists will be created after the user gives the integer.

import java.util.Scanner;

public class TwistGenerator {
  public static void main (String args []) {
    Scanner myScanner = new Scanner ( System.in );
    //get the length as an integer
    System.out.print("Enter the length as an integer: ");
    int length = 0;
    //make sure it is an integer
    if( length == 0 ){
      boolean myTruth = myScanner.hasNextInt();
      while (myTruth == false) {
        String junkWord = myScanner.next();
        System.out.println("Not an integer!");
        System.out.print("Enter the length as an integer: ");
        myTruth = myScanner.hasNextInt();
      }
      length = myScanner.nextInt();
    }
    //length = myScanner.nextInt();
    //int length = myScanner.nextInt();
    //declare all variables
    int lengthFrontSlash = length;
    int lengthX = length;
    int lengthBackslash = length;
    int myRemainderFrontSlash = 0;
    int myRemainderX = 0;
    int myRemainderBackslash = 0;
    
    int counterFrontSlash = -1;
    int counterFrontSlashRem = 0;
    //do case for top row
    while (lengthFrontSlash > 0){
      counterFrontSlash++;
      counterFrontSlashRem = counterFrontSlash % 3;
      switch (counterFrontSlashRem){
        case 0:
          System.out.print("\\");
        break;
        case 1:
          System.out.print(" ");
        break;
        case 2:
          System.out.print("/");
        break;
      }

      lengthFrontSlash--;
    }
    System.out.println("");
    
    int counterX = -1;
    int counterXRem = 0;
    //do case for middle row
    while (lengthX > 0){
      counterX++;
      counterXRem = counterX % 3;
      switch ( counterXRem ){
        case 0:
          System.out.print(" ");
        break;
        case 1:
          System.out.print("X");
        break;
        case 2:
          System.out.print(" ");
        break;
      }
      lengthX--;
    }
    System.out.println("");
    
    int counterBackslash = -1;
    int counterBackslashRem = 0;
    //do case for bottom row
    while (lengthBackslash > 0){
      counterBackslash++;
      counterBackslashRem = counterBackslash % 3;
      switch (counterBackslashRem){
        case 0:
          System.out.print("/");
        break;
        case 1:
          System.out.print(" ");
        break;
        case 2:
          System.out.print("\\");
        break;
      }
      lengthBackslash--;
    }
    System.out.println("");
  }
}