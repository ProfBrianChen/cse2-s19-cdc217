//Chase Clapp
//CSE 002 Section 210
//April 12 2019
//The purpose of the lab is to be able to take arrays and search through them.
//The student should be able to search in a linear fashion and search in binary fashion.
//There should be four methods: one for an array with random integers up to its random length (i.e. length 5, rand ints 0-5),
//one method for generating an array with ascending integers, one method for looking through an array for a certain integer,
//and one method for doing a binary search of an integer.

import java.util.Random;
import java.util.Arrays;

public class Searching{
	
	//make method FirstArray to create random array
	public static int[] myFirstArray(){
		Random myRandom = new Random();
		int myLength = 50;
		int[] myArray = new int[myLength];
		int value = 0;
		for (int i = 0; i < myLength; i++){
			myArray[i] = myRandom.nextInt(myLength);
		}
		return myArray;
	}
	
	//create method SecondArray to create random sorted array
	public static int[] mySecondArray(){
		Random myRandom = new Random();
		int myLength = 50;
		int[] myArray = new int[myLength];
		int value = 0;
		for (int i = 0; i < myLength; i++){
			myArray[i] = myRandom.nextInt(myLength);
		}
		Arrays.sort(myArray);
		for (int j = 0; j < myArray.length; j++){
			System.out.print(myArray[j]+" ");
		}
		System.out.println();
		return myArray;
	}
	
	//create method ThirdArray to look through first array for the integer
	public static void myThirdArray(int[] givenArray, int givenInt){
		int myPlace = -1;
		int k = 0;
		for (int i = 0; i < givenArray.length; i++){
			if (givenArray[i] == givenInt){
				myPlace = i;
			}
		}
		for (int j = 0; j < givenArray.length; j++){
			System.out.print(givenArray[j]+" ");
		}
		System.out.println();
		//print out integer
		System.out.println("Places where integer occurs: "+myPlace);
	}
	
	//create method FourthArray to go through sorted array in binary fashion
	public static void myFourthArray(int arr[], int placeholder){
		int first = arr[0];
		int last = arr[arr.length - 1];
		int mid = (first + last)/2;  
		int that = -1;
		while( first <= last ){  
			if ( arr[mid] < placeholder ){  
				 first = mid + 1;     
			}else if ( arr[mid] == placeholder ){ 
				 that = mid;  
				 break;  
			}else{  
				last = mid - 1;  
			}  
			mid = (first + last)/2;  
		}  
		System.out.println("Place where integer occurs: "+that);
  }
	
	//create public class
	public static void main(String[] args){
		//create random variables
		Random myRandom = new Random();
		int[] myNewArray = myFirstArray();
		//run third array
		int myInteger = myRandom.nextInt(50);
		System.out.println("Your integer is :"+myInteger);
		myThirdArray(myNewArray, myInteger);
		//run fourth array
		int[] givenArray = mySecondArray();
		myFourthArray(givenArray, myInteger);
	}
}