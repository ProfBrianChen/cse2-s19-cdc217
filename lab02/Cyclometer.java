// Chase Clapp
// 2/3/2019
// CSE2 Section 210 Lab 02
//The purpose of the lab is to learn how to code with certain types of numbers, i.e. integers, floats, doubles.
//Given the amount of time, as well as rotation counts of the cycle, the program should be able to:
//print number of minutes, counts, distance (in miles) and combined distance of both trips.

public class Cyclometer {
  public static void main(String[] args) {
    // input data
    int secsTrip1=480; //gives the duration of the first trip in seconds
    int secsTrip2=3220; //gives the duration of the first trip in seconds
    int countsTrip1=1561; //gives the number of rotations during the first trip
    int countsTrip2=9037; //gives the number of rotations during the second trip
    // intermediate values and output data
    double wheelDiameter=27.0, //specifies diameter of the wheels on the bike
    PI=3.14159, //for measuring total wheel length (diameter * PI)
    feetPerMile=5280, //specifies how many feet in one mile
    inchesPerFoot=12, //specifies how many inches in one foot
    secondsPerMinute=60; //specifies how many seconds in one minute
    double distanceTrip1, distanceTrip2,totalDistance; //specifies we need to find these variables
    //print out the given values or new values found from old values put through equations
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");//computes trip 1 mins and prints counts
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");//computes trip 2 mins and prints counts
    //calculations have been run, more calculations are below, finding distances of trips 1 and 2
    // equation is distance = counts * wheel diameter * pi
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //gives distance in inches, now need to go to feet, then to miles
    distanceTrip1/=inchesPerFoot*feetPerMile;//distance has been given in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//all calculations for distance of trip 2
    totalDistance=distanceTrip1+distanceTrip2;
    //print the data
    System.out.println("Trip 1 was "+distanceTrip1+" miles.");
    System.out.println("Trip 2 was "+distanceTrip2+" miles.");
    System.out.println("The total distance was "+totalDistance+" miles.");
  }
    
}