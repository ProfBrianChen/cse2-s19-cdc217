//Chase Clapp

import java.util.Scanner;

public class PatternA{
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in);
    int myNumber = 0;
    boolean myNumberTruth = true;
    String junkWord = "";
    
    //use a do while statement to see if the number is an integer, between (and including) 1 and 10
    do{
      System.out.print("Enter an integer 1-10: ");
      myNumberTruth = myScanner.hasNextInt();
      if (myNumberTruth == true){
        myNumber = myScanner.nextInt();
        if (myNumber > 10 || myNumber < 1){
          myNumberTruth = false;
          System.out.println("Not a correct number!");
        }
      }
      else if (myNumberTruth == false){
        junkWord = myScanner.next();
        System.out.println("Not a correct number!");
      }
    } while (myNumberTruth == false);
    
    //begin code for Pattern A
    int myDif = 2;
    int myCount;
    for (int myLines = 1; myLines < (myNumber + 1); myLines++){
        for (myCount = 1; myCount < myDif; myCount++){
          System.out.print(myCount);
          System.out.print(" ");
        }
      System.out.println("");
      myCount = 1;
      myDif++;
    }
  }
}