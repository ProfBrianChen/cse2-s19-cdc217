//Chase Clapp

import java.util.Scanner;

public class PatternC{
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in);
    int myNumber = 0;
    boolean myNumberTruth = true;
    String junkWord = "";
    
    //use a do while statement to see if the number is an integer, between (and including) 1 and 10
    do{
      System.out.print("Enter an integer 1-10: ");
      myNumberTruth = myScanner.hasNextInt();
      if (myNumberTruth == true){
        myNumber = myScanner.nextInt();
        if (myNumber > 10 || myNumber < 1){
          myNumberTruth = false;
          System.out.println("Not a correct number!");
        }
      }
      else if (myNumberTruth == false){
        junkWord = myScanner.next();
        System.out.println("Not a correct number!");
      }
    } while (myNumberTruth == false);
    
    int myDif = 0;
    int myCount;
    int mySpaces = myNumber - 1;
    int mySpacesRem = myNumber - 1;
    for (int myLines = 1; myLines < (myNumber + 1); myLines++){
      myCount = myLines;
      for (mySpaces = mySpacesRem; mySpaces > 0; mySpaces--){
        System.out.print(" ");
      }
      for (myCount = myLines; myCount > 0; myCount--){
        System.out.print(myCount);
      }
      mySpacesRem--;
      mySpaces = mySpacesRem;
      System.out.println();
      myDif++;
    }
  }
}