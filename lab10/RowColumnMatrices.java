//Chase Clapp
//CSE 002 Section 210
//19 April 2019
//The purpose of the lab is to become better acquainted with row and column major matrices.
//Matrices would then be added together, depending on whether or not they mathed in size and dimension.

import java.util.Random;
	
public class RowColumnMatrices{
	//create method for increasing matrices
	public static int[][] increasingMatrix(int width, int height, boolean format){
		int[][] temp = new int[height][width];
		int[][] falseTemp = new int[width][height];
		int var = 0;
		int i = 0;
		int j = 0;
		if (format == true){//row-major here
			var = 1;
			for (i = 0; i < temp.length; i++){
				for (j = 0; j < temp[0].length; j++){
					temp[i][j] = var;
					var ++;
				}
			}
			return temp;
		}
		else {//column major here
			var = 1;
			for (j = 0; j < temp.length; j++){
				for (i = 0; i < temp[0].length; i++){
					falseTemp[i][j] = var;
					var++;
				}
			}
			return falseTemp;
		}
	}
	//create method for printing matrices
	public static void printMatrix(int[][] temp, boolean myTruth){
		int i = 0;
		int j = 0;
		if (myTruth == true){
			for (i = 0; i < temp.length; i++){
				for (j = 0; j < temp[0].length; j++){
					System.out.printf("%2d ",temp[i][j]);//make sure for right size, also, here is row major
				}
				System.out.println();
			}
		}
		else if (myTruth == false){
			for (j = 0; j < temp[0].length; j++){//here is column major
				for (i = 0; i < temp.length; i++){
					System.out.printf("%2d ",temp[i][j]);
				}
				System.out.println();
			}
		}
	}
	//translate column-major array to row-major array
	public static int[][] translate(int[][] temp){
		int[][] array = new int[temp[0].length][temp.length];
		int[] placeholder = new int[temp.length * temp[0].length];
		int i = 0;
		int j = 0;
		int k = 0;
		for (j = 0; j < temp.length; j++){
			for (i = 0; i < temp[0].length; i++){
				array[i][j] = temp[j][i];
			}
		}
		return array;
	}
	//add matrices based on whether or not they are compatible
	public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb){
		int[][] placeholder = new int[a.length][a[0].length];
		int i = 0; 
		int j = 0;
		if (formata == false){
			a = translate(a);
		}
		if (formatb == false){
			b = translate(b);
		}
		if (a.length != b.length || a[0].length != b[0].length){
			return null;//return null if not true
		}
		else{
			for (i = 0; i < a.length; i++){
				for (j = 0; j < a[0].length; j++){
					placeholder[i][j] = a[i][j] + b[i][j];
				}
			}
			return placeholder;
		}
	}
	//main method
	public static void main (String[] args){
		Random myRandom = new Random();
		//declare all variables
		int width1 = myRandom.nextInt(5) + 1;
		int height1 = myRandom.nextInt(5) + 1;
		int width2 = myRandom.nextInt(5) + 1;
		int height2 = myRandom.nextInt(5) + 1;
		//run methods
		int[][] temp1 = increasingMatrix(width1, height1, true);
		int[][] temp2 = increasingMatrix(width1, height1, false);
		int[][] temp3 = increasingMatrix(width2, height2, true);
		//print matrices
		System.out.println("First matrix: ");
		printMatrix(temp1, true);
		System.out.println("Second matrix: ");
		printMatrix(temp2, false);
		System.out.println("Third matrix: ");
		printMatrix(temp3, true);
		System.out.println("Added matrices 1 and 2: ");
		//add first matrix
		printMatrix(addMatrix(temp1, true, temp2, false), true);
		//see if adding second matrix is possible
		int[][] finalMatrix = addMatrix(temp1, true, temp3, true);
		if (finalMatrix != null){
			System.out.println("Added matrices 1 and 3: ");
			printMatrix(finalMatrix, true);
		}
		else{
			System.out.println("These arrays (1 and 3) cannot be added together!");
		}
	}
}