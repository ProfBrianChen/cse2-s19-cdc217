//Chase Clapp
//CSE 002 Section 210
//4/8/2019
//The purpose of the homework is to sort letters from an array of random chars.
//The letters A through N are to be put in orderand printed from A-N uppercase, then A-N lowercase.
//The remaining letters should be printed in the order they were in from the array.
//There may also be numbers, which also get sorted into the remaining letters, because they are not A-N.

import java.util.Random;


public class Letters{
	
	
	//do a method to get A to M
	public static void getAtoM(char[] input){
		char[] output = new char[13];
		int j = 0;
		for (int i = 0; i <= 12; i++){
			if (input[i] > 64 && input[i] < 78){
				output[j] = input[i];
				j++;
			}
			//make sure to get lowercase
			else if (input[i] > 96 && input[i] < 110){
				output[j] = input[i];
				j++;
			}
			//System.out.println(output);
		}
		System.out.print("AtoM: ");
		System.out.println(output);
	}
	//do a method to get N to Z and numbers
	public static void getNtoZ(char [] input){
		char[] output = new char[13];
		int j = 0;
		for (int i = 0; i <= 12; i++){
			if (input[i] >= 78 && input[i] <= 90){
				output[j] = input[i];
				j++;
			}
			//get lowercase
			else if (input[i] >= 110 && input[i] <= 122){
				output[j] = input[i];
				j++;
			}
			//get numbers
			else if (input[i] >= 48 && input[i] <= 57){
				output[j] = input[i];
				j++;
			}
		}
		System.out.print("NtoZ and Numbers: ");
		System.out.println(output);
	}
	//do the main method
	public static void main (String [] args){
		
		//set what will be available
		char[] options = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9'};
		char[] result =  new char[13];
		Random r = new Random();
		//do random generator
		for(int i=0;i<result.length;i++){
			result[i]=options[r.nextInt(options.length)];
		}
		System.out.println(result);
		//run methods
		getAtoM(result);
		getNtoZ(result);
		
		
	}
	
}