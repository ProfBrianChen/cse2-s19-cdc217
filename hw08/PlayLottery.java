//Chase Clapp
//CSE 002 Section 210
//April 9 2019
//The purpose of this code is to be able to get lottery numbers.
//Given some numbers from a user, compare it to generated numbers from the computer to see if they are similar.
//If all five numbers match, you win.

//import scanner and Random
import java.util.Scanner;
import java.util.Random;

public class PlayLottery{
	//do method to sort numbers picked
	public static int[] numbersPicked (){
		int[] placeholder = new int[5];
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Choose your numbers 0-59: ");
			for (int i = 0; i <= 4; i++){
				placeholder[i] = myScanner.nextInt();
				//System.out.print(", ");
			}
		int temp;
		for (int i = 1; i < placeholder.length; i++) {
		 for (int j = i; j > 0; j--) {
			 if (placeholder[j] < placeholder [j - 1]) {
				 temp = placeholder[j];
				 placeholder[j] = placeholder[j - 1];
				 placeholder[j - 1] = temp;
			}
		}
	}
		for (int i = 0; i < placeholder.length; i++) {
			 System.out.print(placeholder[i]);
			 System.out.print(" ");
		}
		return placeholder;
	}
	
	//public static void int[] userWins {
		
		
		
//		int 
//	}
	//begin main method
	public static void main (String[] args){
		Scanner myScanner = new Scanner(System.in);
		numbersPicked();
		Random myRandom = new Random();
		int guide = 0;
		int j = 0;
		int[] randArray = new int[5];
		//get random numbers from a generator
		for (int i = 0; i <= 4; i++){
			randArray[i] = myRandom.nextInt(59-guide) + guide;
			guide = randArray[i];
			j += randArray[i];
		}
		for (int k = 0; k < randArray.length; k++) {
			 System.out.print(randArray[k]);
			 System.out.print(" ");
		}
	}
}