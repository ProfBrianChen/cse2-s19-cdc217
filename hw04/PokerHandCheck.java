//Chase Clapp
//CSE 002 Section 13
//02/18/2019
//The purpose of this lab is to pick five cards from a deck, 
//and based on those cards, attempt to make poker hads with them, such as
//pair, two pair, three of a kind, or high card.

public class PokerHandCheck{
  public static void main (String[] args){
    String cardNumberRead1;
    String cardSuit1;
    //pick a random number, 1-52
    int cardNumber1 = (int) (Math.random() * 51 + 1);
    //set standards for suits
    if (cardNumber1 >= 1 && cardNumber1 < 14){
        cardSuit1 = "Diamonds";
    }
    else if (cardNumber1 >= 14 && cardNumber1 < 27){
        cardSuit1 = "Clubs";
    }
    else if (cardNumber1 >= 27 && cardNumber1 < 40){
        cardSuit1 = "Hearts";
    }
    else {
        cardSuit1 = "Spades";
    }
    //identify the card remainder, so we can see what number/face the card is
    int cardRem1 = (cardNumber1 + 13) % 13;
    switch ( cardRem1 ){
      case 1:
        cardNumberRead1 = "2";
      break;
      case 2:
        cardNumberRead1 = "3";
      break;
      case 3:
        cardNumberRead1 = "4";
      break;
      case 4:
        cardNumberRead1 = "5";
      break;
      case 5:
        cardNumberRead1 = "6";
      break;
      case 6:
        cardNumberRead1 = "7";
      break;
      case 7:
        cardNumberRead1 = "8";
      break;
      case 8:
        cardNumberRead1 = "9";
      break;
      case 9:
        cardNumberRead1 = "10";
      break;
      case 10:
        cardNumberRead1 = "Jack";
      break;
      case 11:
        cardNumberRead1 = "Queen";
      break;
      case 12:
        cardNumberRead1 = "King";
      break;
      default:
        cardNumberRead1 = "Ace";
      break;
    }
    String cardNumberRead2;
    String cardSuit2;
    //pick a random number, 1-52
    int cardNumber2 = (int) (Math.random() * 51 + 1);
    //set standards for suits
    if (cardNumber2 >= 1 && cardNumber2 < 14){
        cardSuit2 = "Diamonds";
    }
    else if (cardNumber2 >= 14 && cardNumber2 < 27){
        cardSuit2 = "Clubs";
    }
    else if (cardNumber2 >= 27 && cardNumber2 < 40){
        cardSuit2 = "Hearts";
    }
    else {
        cardSuit2 = "Spades";
    }
    //identify the card remainder, so we can see what number/face the card is
    int cardRem2 = (cardNumber2 + 13) % 13;
    switch ( cardRem2 ){
      case 1:
        cardNumberRead2 = "2";
      break;
      case 2:
        cardNumberRead2 = "3";
      break;
      case 3:
        cardNumberRead2 = "4";
      break;
      case 4:
        cardNumberRead2 = "5";
      break;
      case 5:
        cardNumberRead2 = "6";
      break;
      case 6:
        cardNumberRead2 = "7";
      break;
      case 7:
        cardNumberRead2 = "8";
      break;
      case 8:
        cardNumberRead2 = "9";
      break;
      case 9:
        cardNumberRead2 = "10";
      break;
      case 10:
        cardNumberRead2 = "Jack";
      break;
      case 11:
        cardNumberRead2 = "Queen";
      break;
      case 12:
        cardNumberRead2 = "King";
      break;
      default:
        cardNumberRead2 = "Ace";
      break;
    }
    String cardNumberRead3;
    String cardSuit3;
    //pick a random number, 1-52
    int cardNumber3 = (int) (Math.random() * 51 + 1);
    //set standards for suits
    if (cardNumber3 >= 1 && cardNumber3 < 14){
        cardSuit3 = "Diamonds";
    }
    else if (cardNumber3 >= 14 && cardNumber3 < 27){
        cardSuit3 = "Clubs";
    }
    else if (cardNumber3 >= 27 && cardNumber3 < 40){
        cardSuit3 = "Hearts";
    }
    else {
        cardSuit3 = "Spades";
    }
    //identify the card remainder, so we can see what number/face the card is
    int cardRem3 = (cardNumber3 + 13) % 13;
    switch ( cardRem3 ){
      case 1:
        cardNumberRead3 = "2";
      break;
      case 2:
        cardNumberRead3 = "3";
      break;
      case 3:
        cardNumberRead3 = "4";
      break;
      case 4:
        cardNumberRead3 = "5";
      break;
      case 5:
        cardNumberRead3 = "6";
      break;
      case 6:
        cardNumberRead3 = "7";
      break;
      case 7:
        cardNumberRead3 = "8";
      break;
      case 8:
        cardNumberRead3 = "9";
      break;
      case 9:
        cardNumberRead3 = "10";
      break;
      case 10:
        cardNumberRead3 = "Jack";
      break;
      case 11:
        cardNumberRead3 = "Queen";
      break;
      case 12:
        cardNumberRead3 = "King";
      break;
      default:
        cardNumberRead3 = "Ace";
      break;
    }
    String cardNumberRead4;
    String cardSuit4;
    //pick a random number, 1-52
    int cardNumber4 = (int) (Math.random() * 51 + 1);
    //set standards for suits
    if (cardNumber4 >= 1 && cardNumber4 < 14){
        cardSuit4 = "Diamonds";
    }
    else if (cardNumber4 >= 14 && cardNumber4 < 27){
        cardSuit4 = "Clubs";
    }
    else if (cardNumber4 >= 27 && cardNumber4 < 40){
        cardSuit4 = "Hearts";
    }
    else {
        cardSuit4 = "Spades";
    }
    //identify the card remainder, so we can see what number/face the card is
    int cardRem4 = (cardNumber4 + 13) % 13;
    switch ( cardRem4 ){
      case 1:
        cardNumberRead4 = "2";
      break;
      case 2:
        cardNumberRead4 = "3";
      break;
      case 3:
        cardNumberRead4 = "4";
      break;
      case 4:
        cardNumberRead4 = "5";
      break;
      case 5:
        cardNumberRead4 = "6";
      break;
      case 6:
        cardNumberRead4 = "7";
      break;
      case 7:
        cardNumberRead4 = "8";
      break;
      case 8:
        cardNumberRead4 = "9";
      break;
      case 9:
        cardNumberRead4 = "10";
      break;
      case 10:
        cardNumberRead4 = "Jack";
      break;
      case 11:
        cardNumberRead4 = "Queen";
      break;
      case 12:
        cardNumberRead4 = "King";
      break;
      default:
        cardNumberRead4 = "Ace";
      break;
    }
    String cardNumberRead5;
    String cardSuit5;
    //pick a random number, 1-52
    int cardNumber5 = (int) (Math.random() * 51 + 1);
    //set standards for suits
    if (cardNumber5 >= 1 && cardNumber5 < 14){
        cardSuit5 = "Diamonds";
    }
    else if (cardNumber5 >= 14 && cardNumber5 < 27){
        cardSuit5 = "Clubs";
    }
    else if (cardNumber5 >= 27 && cardNumber5 < 40){
        cardSuit5 = "Hearts";
    }
    else {
        cardSuit5 = "Spades";
    }
    //identify the card remainder, so we can see what number/face the card is
    int cardRem5 = (cardNumber5 + 13) % 13;
    switch ( cardRem5 ){
      case 1:
        cardNumberRead5 = "2";
      break;
      case 2:
        cardNumberRead5 = "3";
      break;
      case 3:
        cardNumberRead5 = "4";
      break;
      case 4:
        cardNumberRead5 = "5";
      break;
      case 5:
        cardNumberRead5 = "6";
      break;
      case 6:
        cardNumberRead5 = "7";
      break;
      case 7:
        cardNumberRead5 = "8";
      break;
      case 8:
        cardNumberRead5 = "9";
      break;
      case 9:
        cardNumberRead5 = "10";
      break;
      case 10:
        cardNumberRead5 = "Jack";
      break;
      case 11:
        cardNumberRead5 = "Queen";
      break;
      case 12:
        cardNumberRead5 = "King";
      break;
      default:
        cardNumberRead5 = "Ace";
      break;
    }
    int myPairValue = 0;
    int myThreeValue = 0;
    boolean myPair1 = (cardNumberRead1 == cardNumberRead2 || cardNumberRead1 == cardNumberRead3 || cardNumberRead1 == cardNumberRead4 || cardNumberRead1 == cardNumberRead5);
    if (myPair1 == true) {
      myPairValue ++;
    }
    boolean myPair2 = (cardNumberRead2 == cardNumberRead3 || cardNumberRead2 == cardNumberRead4 || cardNumberRead2 == cardNumberRead5);
    if (myPair2 == true && cardNumberRead2 == cardNumberRead1){
     myThreeValue ++;
    }
    else if (myPair2 == true && cardNumberRead2 != cardNumberRead1) {
    myPairValue ++;     
    }
    boolean myPair3 = (cardNumberRead3 == cardNumberRead4 || cardNumberRead3 == cardNumberRead5);
    if (myPair3 == true && (cardNumberRead3 == cardNumberRead1 || cardNumberRead3 == cardNumberRead2)){
    myThreeValue ++;
    }
    else if (myPair3 == true && (cardNumberRead3 != cardNumberRead1 || cardNumberRead3 != cardNumberRead2)) {
    myPairValue ++;     
    }
    boolean myPair4 = (cardNumberRead4 == cardNumberRead5);
    if (myPair4 == true && (cardNumberRead4 == cardNumberRead1 || cardNumberRead4 == cardNumberRead2 || cardNumberRead4 == cardNumberRead3 || cardNumberRead5 == cardNumberRead1 || cardNumberRead5 == cardNumberRead2 || cardNumberRead5 == cardNumberRead3)){
    myThreeValue ++;
    }
    else if (myPair4 == true && (cardNumberRead4 != cardNumberRead1 || cardNumberRead4 != cardNumberRead2 || cardNumberRead4 != cardNumberRead3 || cardNumberRead4 != cardNumberRead3 || cardNumberRead5 != cardNumberRead1 || cardNumberRead5 != cardNumberRead2 || cardNumberRead5 != cardNumberRead3)) {
    myPairValue ++;     
    }
    String myHandName;
    String pairIntro;

    switch ( myPairValue ){
      case 1:
        myHandName = "a pair!";
      break;
      case 2:
        myHandName = "two pair!";
      break;      
      default:
        myHandName = "a high card hand!";
      break;
    }
    switch ( myThreeValue ){
      case 1:
        myHandName = "You have three of a kind!";
      break;
      default:
        pairIntro = "You have ";
    }
    System.out.println("Your random cards were:");
    System.out.println("the "+cardNumberRead1+" of "+cardSuit1+".");
    System.out.println("the "+cardNumberRead2+" of "+cardSuit2+".");
    System.out.println("the "+cardNumberRead3+" of "+cardSuit3+".");
    System.out.println("the "+cardNumberRead4+" of "+cardSuit4+".");
    System.out.println("the "+cardNumberRead5+" of "+cardSuit5+".");
    System.out.println("You have "+myHandName);
  }
}