//Chase Clapp
import java.util.Scanner;
import java.util.Random;
public class Methods{
  
  public static String adjectives (int adjInput){
		String adjReturn = "";
		switch (adjInput){
			case 0:
				adjReturn = "small";
			case 1:
				adjReturn = "good";
			break;
			case 2:
				adjReturn = "fluffy";
			break;
			case 3:
				adjReturn = "hard";
			break;
			case 4:
				adjReturn = "accepting";
			break;
			case 5:
				adjReturn = "cruel";
			break;
			case 6:
				adjReturn = "athletic";
			break;
			case 7:
				adjReturn = "boring";
			break;
			case 8:
				adjReturn = "subtle";
			break;
			case 9:
				adjReturn = "cataclysmic";
			break;
		}
		return adjReturn;
	}
	
	 public static String subjects (int subjectInput){
		String subjectNoun = "";
		switch (subjectInput){
			case 0:
				subjectNoun = "parrot";
			case 1:
				subjectNoun = "cat";
			break;
			case 2:
				subjectNoun = "dog";
			break;
			case 3:
				subjectNoun = "human";
			break;
			case 4:
				subjectNoun = "whale";
			break;
			case 5:
				subjectNoun = "fish";
			break;
			case 6:
				subjectNoun = "snail";
			break;
			case 7:
				subjectNoun = "eagle";
			break;
			case 8:
				subjectNoun = "hawk";
			break;
			case 9:
				subjectNoun = "lizard";
			break;
		}
		 return subjectNoun;
	}
	
	 public static String verbs (int verbInput){
		String verbReturn = "";
		switch (verbInput){
			case 0:
				verbReturn = "befriended";
			case 1:
				verbReturn = "ate";
			break;
			case 2:
				verbReturn = "threw";
			break;
			case 3:
				verbReturn = "ran from";
			break;
			case 4:
				verbReturn = "hid from";
			break;
			case 5:
				verbReturn = "carried";
			break;
			case 6:
				verbReturn = "shoved";
			break;
			case 7:
				verbReturn = "looked at";
			break;
			case 8:
				verbReturn = "brushed";
			break;
			case 9:
				verbReturn = "smiled at";
			break;
		}
		 return verbReturn;
	}
	
	 public static String pretiquette (int pretInput){
		String pretReturn = "";
		switch (pretInput){
			case 0:
				pretReturn = "speaker";
			case 1:
				pretReturn = "box";
			break;
			case 2:
				pretReturn = "lamp";
			break;
			case 3:
				pretReturn = "wallet";
			break;
			case 4:
				pretReturn = "phone";
			break;
			case 5:
				pretReturn = "carpet";
			break;
			case 6:
				pretReturn = "hat";
			break;
			case 7:
				pretReturn = "pencil";
			break;
			case 8:
				pretReturn = "notebook";
			break;
			case 9:
				pretReturn = "bottle";
			break;
		}
		 return pretReturn;
	}
		
	/*
	public static String subjectGive(){
		
		Random randomGenerator = new Random();
		int number = 0;
		String firstWord = "";
		String secondWord = "";
		String thirdWord = "";
		String fourthWord = "";
		String fifthWord = "";
		for (int wordNumber = 1; wordNumber < 6; wordNumber++){
			number = randomGenerator.nextInt(10);
			if (wordNumber == 1){
				firstWord = adjectives(number);
			}
			else if (wordNumber == 2){
				secondWord = subjects(number);
			}
			else if (wordNumber == 3){
				thirdWord = verbs(number);
			}
			else if (wordNumber == 4){
				fourthWord = adjectives(number);
			}
			else if (wordNumber == 5){
				fifthWord = pretiquette(number);
			}
		}
		System.out.println("The "+firstWord+" "+secondWord+" "+thirdWord+" the "+fourthWord+" "+fifthWord);
		return secondWord;
	}
	
	public static void paragraph(){
		String givenSub = subjectGive();
		Random randomGenerator2 = new Random();
		int myBeginning = randomGenerator2(3);
		String beginSentence = ";"
		switch (myBeginning);
		case 0:
			beginSentence = "It";
			System.out.print(beginSentence);
		break;
		case 1: 
			beginSentence = "The ";
			System.out.print(beginSentence+givenSub);
	}
	*/
	
  public static void main (String args[]){
		Scanner myScanner = new Scanner(System.in);
		Random randomGenerator = new Random();
		int number = 0;
		String firstWord = "";
		String secondWord = "";
		String thirdWord = "";
		String fourthWord = "";
		String fifthWord = "";
		boolean myTruth = true;
		while (myTruth == true){
			for (int wordNumber = 1; wordNumber < 6; wordNumber++){
				number = randomGenerator.nextInt(10);
				if (wordNumber == 1){
					firstWord = adjectives(number);
				}
				else if (wordNumber == 2){
					secondWord = subjects(number);
				}
				else if (wordNumber == 3){
					thirdWord = verbs(number);
				}
				else if (wordNumber == 4){
					fourthWord = adjectives(number);
				}
				else if (wordNumber == 5){
					fifthWord = pretiquette(number);
				}
				
			}
			System.out.println("The "+firstWord+" "+secondWord+" "+thirdWord+" the "+fourthWord+" "+fifthWord);
			
			
			
			boolean myTrivialTruth = false;
			
			while (myTrivialTruth == false){
				System.out.print("Do you want another sentence? yes or no: ");
				String myAnswer = myScanner.nextLine();
				if (myAnswer.equals("yes")||myAnswer.equals("no")){
					if (myAnswer.equals("yes")){
						myTrivialTruth = true;
						myTruth = true;
					}
					else if (myAnswer.equals("no")){
						myTrivialTruth = true;
						myTruth = false;
					}
				}
				else{
					System.out.println("Not a correct input");
					myTrivialTruth = false;
				}
			}
		}
		
		
		
		/*
		int firstRand = randomGenerator.nextInt(10);
		int secondRand = randomGenerator.nextInt(10);
		int thirdRand = randomGenerator.nextInt(10);
		int fourthRand = randomGenerator.nextInt(10);
		int fifthRand = randomGenerator.nextInt(10);
		String firstWord = adjectives(firstRand);
		String secondWord = subjects(secondRand);
		String thirdWord = verbs(thirdRand);
		String fourthWord = adjectives(fourthRand);
		String fifthWord = pretiquette(fifthRand);
		*/
		//System.out.println("The "+firstWord+" "+secondWord+" "+thirdWord+" the "+fourthWord+" "+fifthWord);
    

    
  }
	
	//
}